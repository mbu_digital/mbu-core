<?php declare(strict_types=1);

namespace mbu\tools\Ui\DataProvider;

use mbu\tools\Model\ResourceModel\Banners\Collection;
use mbu\tools\Model\ResourceModel\Banners\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;

class Banners extends AbstractDataProvider
{
    /**
     * @var $collection
     */
    protected $collection;

    /**
     * @var array
     */
    private array $loadedData;

    protected $request;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        Collection $collection,
        \Magento\Framework\App\RequestInterface $request,
        array $meta = [],
        array $data = [],
    ) {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->request = $request;
        foreach ($this->collection->getItems() as $item) {
            $this->loadedData[$item->getData('id')] = $item->getData();
        }
    }
    protected function getCurrentBannerId()
    {
        return $this->request->getParam('id');
    }
    public function getMeta()
    {
        $meta = parent::getMeta();

        $banner = $this->loadedData[$this->getCurrentBannerId()] ?? null;
        if ($banner) {
            $meta['mbu_tools_banners_fieldset']['children']['editing_info']['arguments']['data']['config']['content'] = __('{{block class="mbu\tools\Block\CustomBlock" template="mbu_tools::custom_template.phtml" id=" %1 "}}', $banner['id']);
        }

        return $meta;
    }
    /**
     * @return array
     */
    public function getData() : array
    {
        if (!isset($this->loadedData)) {
            $this->loadedData = [];

            foreach ($this->collection->getItems() as $item) {
                $this->loadedData[$item->getData('id')] = $item->getData();
            }
        }
        return $this->loadedData;
    }
}
