<?php
namespace mbu\tools\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

class Shortcode extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['id'])) {
                    $item[$this->getData('name')] = '{{block class="mbu\tools\Block\CustomBlock" template="mbu_tools::custom_template.phtml" id="' . $item['id'].'"}}';
                }
            }
        }

        return $dataSource;
    }
}
