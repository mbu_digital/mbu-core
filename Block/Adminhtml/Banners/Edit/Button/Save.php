<?php declare(strict_types=1);

namespace mbu\tools\Block\Adminhtml\Banners\Edit\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Save implements ButtonProviderInterface
{
    /**
     * Returns button data.
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save'),
            'class' => 'save primary',
            'data_attribute' =>
                [
                'mage-init' =>
                    [
                    'button' =>
                        [
                        'event' => 'save',
                    ],
                ],
            ],
        ];
    }
}
