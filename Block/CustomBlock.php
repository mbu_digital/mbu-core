<?php declare(strict_types=1);
namespace mbu\tools\Block;

use Magento\Cms\Model\Template\FilterProvider;
use Magento\Framework\View\Element\Template;
use mbu\tools\Model\ResourceModel\Banners\CollectionFactory;
use mbu\tools\Model\ResourceModel\Banners\Collection;

class CustomBlock extends Template
{
    /**
     * @var $collection
     */
    protected $collection;
    /**
     * @var $filterProvider
     */
    protected $filterProvider;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param Template\Context $context
     * @param CollectionFactory $collectionFactory
     * @param Collection $collection
     * @param FilterProvider $filterProvider
     * @param array $data
     */

    public function __construct(
        Template\Context $context,
        CollectionFactory $collectionFactory,
        Collection $collection,
        FilterProvider $filterProvider,
        array $data = [],
    ) {
        parent::__construct($context, $data);
        $this->collectionFactory = $collectionFactory;
        $this->setId($data['id']);
        $this->_filterProvider = $filterProvider;
    }
    /**
     * Retrieves custom data based on ID.
     *
     * @return array
     * @throws \Exception
     */
    public function getCustomData()
    {
        $id=$this->getId();
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('id', $id);
        $collection->load();
        $content= $collection->getData();
        $block[]='';
        foreach ($content as $data) {
            $block['desktop']=$this->_filterProvider->getPageFilter()->filter($data['banner']);
            $block['mobile']=$this->_filterProvider->getPageFilter()->filter($data['banner2']);
            $block['url']=$data['href'];
        }

        return $block;
    }
}
