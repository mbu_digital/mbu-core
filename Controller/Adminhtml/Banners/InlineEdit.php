<?php declare(strict_types=1);

namespace mbu\tools\Controller\Adminhtml\Banners;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Setup\Exception;
use mbu\tools\Model\Banners;
use mbu\tools\Model\BannersFactory;
use mbu\tools\Model\ResourceModel\Banners as BannerResource;

class InlineEdit extends Action implements HttpPostActionInterface
{
    const ADMIN_RESOURCE = 'Mbu_Tools::banners_save';

    /**
     * @var $jsonFactory
     */
    protected $jsonFactory;

    /**
     * @var $BannersFactory
     */
    protected $BannersFactory;
    /**
     * @var $BannerResource
     */
    protected $BannerResource;
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        BannersFactory $bannersFactory,
        BannerResource $bannerResource,
    ) {
        parent::__construct($context);
        $this->jsonFactory= $jsonFactory;
        $this->BannerResource = $bannerResource;
        $this->BannersFactory = $bannersFactory;
    }

    public function execute()
    {
        $json=$this->jsonFactory->create();
        $messages = [];
        $error = false;
        $isAjax= $this->getRequest()->getParam('isAjax', false);
        $items = $this->getRequest()->getParam('items', []);

        if (!$isAjax || !count($items)) {
            $messages[]=__('Please correct the data sent.');
            $error = true;
        }

        if (!$error) {
            foreach ($items as $item) {
                $id = $item['id'];
                try {
                    $banners = $this->BannersFactory->create();
                    $this->BannerResource->load($banners, $id);
                    $banners->setData(array_merge($banners->getData(), $item));
                    $this->BannerResource->save($banners);
                } catch (Exception $e) {
                    $messages[]=__("Something went wrong while saving item $id");
                    $error = true;
                }
            }
        }
        return $json->setData([
            'message' => $messages,
            'error' => $error,
            ]);
    }
}
