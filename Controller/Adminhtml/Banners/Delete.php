<?php declare(strict_types=1);

namespace mbu\tools\Controller\Adminhtml\Banners;

use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use mbu\tools\Model\BannersFactory;
use mbu\tools\Model\ResourceModel\Banners as BannersResource;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;

class Delete extends Action implements HttpGetActionInterface
{
    const ADMIN_RESOURCE = 'Mbu_Tools::banners_delete';

    /**
     * @var $bannersFactory
     */
    protected $bannersFactory;
    /**
     * @var $bannersResource
     */
    protected $bannersResource;


    public function __construct(
        Context $context,
        BannersFactory $bannersFactory,
        BannersResource $bannersResource
    ) {
        $this->bannersResource=$bannersResource;
        $this->bannersFactory =$bannersFactory;

        parent::__construct($context);
    }
    public function execute(): Redirect
    {
        try {
            $id = $this->getRequest()->getParam('id');
            $banner = $this->bannersFactory->create();
            $this->bannersResource->load($banner, $id);
            if ($banner->getId()) {
                $this->bannersResource->delete($banner);
                $this->messageManager->addSuccessMessage(__('The record has been deleted.'));
            } else {
                $this->messageManager->addErrorMessage(__('The record does not exist.'));
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        /**
         * @var Redirect $redirect
         */
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $redirect->setPath('*/*');
    }
}
