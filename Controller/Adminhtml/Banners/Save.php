<?php declare(strict_types=1);

namespace mbu\tools\Controller\Adminhtml\Banners;

use mbu\tools\Model\Banners;
use mbu\tools\Model\BannersFactory;
use mbu\tools\Model\ResourceModel\Banners as BannersResource;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NotFoundException;

class Save extends Action implements HttpPostActionInterface
{
    const ADMIN_RESOURCE = 'Mbu_Tools::banners_save';

    /** @var BannersFactory */
    private BannersFactory $bannersFactory;

    /** @var BannersResource */
    private BannersResource $bannersResource;

    /**
     * @param Context $context
     * @param BannersFactory $bannersFactory
     * @param BannersResource $bannersResource
     */
    public function __construct(
        Context $context,
        BannersFactory $bannersFactory,
        BannersResource $bannersResource
    ) {
        $this->bannersFactory = $bannersFactory;
        $this->bannersResource = $bannersResource;
        parent::__construct($context);
    }

    /**
     * @return Redirect
     */
    public function execute(): Redirect
    {
        $post = $this->getRequest()->getPost();
        $isExistingPost = $post->id;
        /** @var Banners $banners */
        $banners = $this->bannersFactory->create();
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if ($isExistingPost) {
            try {
                $this->bannersResource->load($banners, $post->id);
                if (!$banners->getData('id')) {
                    throw new NotFoundException(__('This record no longer exists.'));
                }
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $redirect->setPath('*/*/');
            }
        } else {
            // If new, build an object with the posted data to save it
            unset($post->id);
        }

        $banners->setData(array_merge($banners->getData(), $post->toArray()));

        try {
            $this->bannersResource->save($banners);
            $this->messageManager->addSuccessMessage(__('The record has been saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the record.'));
            return $redirect->setPath('*/*/');
        }

        // On success, redirect back to the admin grid
        return $redirect->setPath('*/*/index');
    }
}
