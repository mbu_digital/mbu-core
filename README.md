-Mbu Tools, is a magento 2.3+ module, that helps you to create and organize banners easily using shortcodes, with mobile/desktop banner switch built in.

-We are working hard to create more useful modules, update them and maintain them.
If you have a suggestion that you think would be useful, please let us know, we would like to improve our module.
visit mbu.digital for suggestions

-The module is free and open source. Look at the GNU licenses for more information.
The GNU license should be deliverd with the moudle do not remove or edit!. 
Although you are not allowed to resell the module or change the vendor name, you can edit the code and also request to upload your changes. We are not responsible for any damage caused by the module, or in the event of a malfunction, the module is delivered as is, and therefore we advise you to check the update log before updating, and use it at your own risk!

-How to install:
1- composer require mbu/tools:dev-master
2- php bin/magento module:enable Mbu_Tools
3- php bin/magento c:f
4- php bin/magento s:up
5-you can acces the module under content page Mbu/Banners 

-How to use:
	after you create your banner by clicking add new banner you can see the shortcode on the screen or when you edit the record copy this shortcode and add it to your template cms page or block, or phtml files. Don't forget to clear the cache afterwards to see the changes.