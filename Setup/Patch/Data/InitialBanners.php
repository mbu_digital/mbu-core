<?php declare(strict_types=1);

namespace mbu\tools\Setup\Patch\Data;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use mbu\tools\Model\ResourceModel\Banners;

class InitialBanners implements DataPatchInterface
{

    /** @var ModuleDataSetupInterface */
    protected $moduleDataSetup;

    /** @var ResourceConnection */
    protected $resource;

    /**
     * InitialFaqs constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ResourceConnection $resource
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        ResourceConnection $resource
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->resource = $resource;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply(): self
    {
        $connection = $this->resource->getConnection();
        $data =
        [
            [
                'title'=>'Black Friday sales',
                'banner'=>'test',
                'banner2'=>'test23',
                'href'=>'url',
            ],
            [
                'title'=>'summer sales',
                'banner'=>'test2',
                'banner2'=>'test23',
                'href'=>'url',
            ],
        ];
        $connection->insertMultiple(Banners::MAIN_TABLE, $data);
        return $this;
    }
}
