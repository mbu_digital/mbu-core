<?php
/**
 * Copyright © MBU/Devlock All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace mbu\tools\Model\Cache;

class Banners extends \Magento\Framework\Cache\Frontend\Decorator\TagScope
{

    const TYPE_IDENTIFIER = 'banner_cache_tag';
    const CACHE_TAG = 'BANNERS_CACHE_TAG';

    /**
     * @param \Magento\Framework\App\Cache\Type\FrontendPool $cacheFrontendPool
     */
    public function __construct(
        \Magento\Framework\App\Cache\Type\FrontendPool $cacheFrontendPool
    ) {
        parent::__construct($cacheFrontendPool->get(self::TYPE_IDENTIFIER), self::CACHE_TAG);
    }
}
