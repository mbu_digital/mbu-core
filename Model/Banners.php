<?php declare(strict_types=1);

namespace mbu\tools\Model;

use Magento\Framework\Model\AbstractModel;

class Banners extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(ResourceModel\Banners::class);
    }
}
