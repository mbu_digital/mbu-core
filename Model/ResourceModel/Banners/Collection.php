<?php declare(strict_types=1);
namespace mbu\tools\Model\ResourceModel\Banners;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use mbu\tools\Model\Banners;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(Banners::class, \mbu\tools\Model\ResourceModel\Banners::class);
    }
}
