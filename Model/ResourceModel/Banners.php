<?php declare(strict_types=1);

namespace mbu\tools\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Banners extends AbstractDb
{
    const MAIN_TABLE ='mbu_tools_banners';
    const ID_FIELD_NAME='id';
    protected function _construct()
    {
        $this->_init(self::MAIN_TABLE, self::ID_FIELD_NAME);
    }
}
